package agh.ics.oop.simulation.gui;

import agh.ics.oop.simulation.map.Animal;
import agh.ics.oop.simulation.map.Grass;
import agh.ics.oop.simulation.map.IMapElement;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class GuiElementBox {

    private final VBox vBox;

    public GuiElementBox(WorldVisualisator worldSimulator, IMapElement element) {
        StackPane pane = new StackPane();

        if(element instanceof Grass) {
            pane.getChildren().add(getGrassImage((Grass) element));
        }

        vBox = new VBox();
        if (element instanceof Animal animal) {
            Circle circle = new Circle(0, 0, 5);
            circle.setFill(Color.LIGHTGRAY);

            if(worldSimulator.isShowDominantAnimals() &&
                    animal.getGenes().equals(worldSimulator.getSimulationMap().getDominantGenotype())) {
                circle.setFill(Color.LIME);
            }

            pane.getChildren().addAll(circle, getLabel(animal));

            vBox.setOnMouseClicked((event) -> {
                worldSimulator.getSimulationEngine().setObservedAnimal(animal);
                System.out.println("Wybrano do obserwowania" + animal.getId() + " --- " +
                        animal.getMap().getMapElementsAt(animal.getPosition()).size());
                worldSimulator.updateAnimalDetailedBox(animal);
                animal.getMap().setObservedAnimalDescendantAmount(0);
                animal.getMap().getAnimals().forEach((s) -> s.setObserved(false));
                animal.setObserved(true);
            });
        }
        vBox.getChildren().add(pane);
        vBox.setAlignment(Pos.CENTER);
    }

    private Text getLabel(Animal animal) {
        String label = Math.min(animal.getEnergy(), 99) + "";
        Text text = new Text(label);
        Font f = new Font("Arial", 8);
        if (animal.getEnergy() < 10) {
            text.setFill(Color.RED);
        }else {
            text.setFill(Color.GREEN);
        }
        text.setFont(f);
        return text;
    }

    public VBox getvBox() {
        return vBox;
    }

    private ImageView getGrassImage(Grass grass) {
        Image image = grass.getImage();
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(10);
        imageView.setFitWidth(10);
        imageView.setOpacity(0.2);
        imageView.setOpacity(1);
        return imageView;
    }
}
