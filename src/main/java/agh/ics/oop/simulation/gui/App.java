package agh.ics.oop.simulation.gui;

import agh.ics.oop.simulation.Config;
import agh.ics.oop.simulation.SimulationEngine;
import agh.ics.oop.simulation.map.SimulationMap;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class App extends Application {

    private TextField mapWidth, mapHeight, startEnergy, moveEnergy, plantEnergy, daysToShow, startAmountOfFood, startAnimalsAmount, dayDuration;
    private Scene mainScene;
    private Slider jungleSlider;
    private WorldVisualisator left, right;

    @Override
    public void start(Stage primaryStage) {
        VBox vBox = new VBox();
        mainScene = new Scene(vBox, 800, 500);
        Label titleLabel = new Label("SimulationEngine v0.1");
        titleLabel.setFont(new Font("Arial", 24));
        primaryStage.setTitle("Simulation engine");

        vBox.getChildren().add(titleLabel);

        generateFields(vBox);

        Button startSimulation = new Button("Run simulation");
        startSimulation.setOnAction((value) -> {
            System.out.println("Running our simulation...");
            runSimulation(primaryStage);
        });

        vBox.getChildren().add(startSimulation);

        primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    @Override
    public void stop() {
        System.out.println("Stopping simulation...");
        left.stopSimulation();
        right.stopSimulation();
        System.out.println("Completed! Good bye :)");
        System.exit(0);
    }

    private WorldVisualisator getWorldSimulator(int mapWidth, int mapHeight, double jungleRatio, int startAnimalsAmount, int dayDuration, boolean solidWalls) {
        SimulationMap simulationMap = new SimulationMap(mapWidth, mapHeight, (float) jungleRatio/100f, solidWalls);
        SimulationEngine simulationEngine = new SimulationEngine(simulationMap);
        simulationMap.spawnFood(Config.getStartAmountOfFood());
        simulationMap.spawnManyAnimals(startAnimalsAmount, Config.getStartEnergy());
        return new WorldVisualisator(simulationMap, simulationEngine, dayDuration);
    }

    private TextField getCustomTextField(VBox container, String description, int defaultValue) {
        Label label1 = new Label(description);
        TextField value = new TextField(String.valueOf(defaultValue));
        container.getChildren().addAll(label1, value);
        return value;
    }

    private void generateFields(VBox vBox) {

        mapWidth = getCustomTextField(vBox, "Width:", 30);
        mapHeight = getCustomTextField(vBox,"Height:", 15);
        startEnergy = getCustomTextField(vBox, "Start energy:", 50);
        moveEnergy = getCustomTextField(vBox, "Move energy:", 1);
        plantEnergy = getCustomTextField(vBox, "Plant energy:", 20);
        daysToShow = getCustomTextField(vBox, "Days to show on chart:", 500);
        startAmountOfFood = getCustomTextField(vBox, "Start grass amount:", 0);
        startAnimalsAmount = getCustomTextField(vBox, "Start animals amount:", 15);
        dayDuration = getCustomTextField(vBox, "Day duration (in millis):", 100);

        Label jungleLabel = new Label("Jungle ratio (%):");
        jungleSlider = new Slider(0, 100, 20);
        jungleSlider.setMajorTickUnit(10);
        jungleSlider.setMinorTickCount(5);
        jungleSlider.setShowTickLabels(true);
        jungleSlider.setShowTickMarks(true);
        jungleSlider.setSnapToTicks(true);
        vBox.getChildren().addAll(jungleLabel, jungleSlider);
    }

    private void runSimulation(Stage primaryStage) {
        int width = Integer.parseInt(mapWidth.getText());
        int height = Integer.parseInt(mapHeight.getText());
        double jungleRatio = jungleSlider.getValue();
        Config.setStartEnergy(Integer.parseInt(startEnergy.getText()));
        int startAnimals = Integer.parseInt(startAnimalsAmount.getText());
        Config.setStartAmountOfFood(Integer.parseInt(startAmountOfFood.getText()));
        int dayDurationValue = Integer.parseInt(dayDuration.getText());
        Config.setMoveEnergy(Integer.parseInt(moveEnergy.getText()));
        Config.setPlantEnergy(Integer.parseInt(plantEnergy.getText()));
        Config.setChartShownDays(Integer.parseInt(daysToShow.getText()));
        HBox maps = new HBox();

        left = this.getWorldSimulator(width, height, jungleRatio, startAnimals, dayDurationValue, false);
        right = this.getWorldSimulator(width, height, jungleRatio, startAnimals, dayDurationValue, true);

        Button backButton = new Button("Back");
        backButton.setOnAction((e)->{
            left.stopSimulation();
            right.stopSimulation();
            primaryStage.setScene(mainScene);
        });

        maps.getChildren().addAll(left.getContainer(), right.getContainer(), backButton);

        Scene s2 = new Scene(maps, 1280, 720);
        primaryStage.setScene(s2);
    }



}
