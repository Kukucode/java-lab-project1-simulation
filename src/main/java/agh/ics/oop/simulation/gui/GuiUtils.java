package agh.ics.oop.simulation.gui;

import agh.ics.oop.simulation.Utils;
import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class GuiUtils {

    private static Image notGrass, notGrassJungle;

    public static Image getNotGrass() {
        if (notGrass == null) {
            try {
                notGrass = new Image(new FileInputStream(Utils.MAIN_PATH + "non_grass2.png"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return notGrass;
    }

    public static Image getNotGrassJungle() {
        if (notGrassJungle == null) {
            try {
                notGrassJungle = new Image(new FileInputStream(Utils.MAIN_PATH + "non_grass2_jungle.png"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return notGrassJungle;
    }

}
