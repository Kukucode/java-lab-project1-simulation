package agh.ics.oop.simulation.gui;

import agh.ics.oop.simulation.Config;
import agh.ics.oop.simulation.SimulationEngine;
import agh.ics.oop.simulation.WriterCSV;
import agh.ics.oop.simulation.map.*;
import javafx.application.Platform;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Synchronized;

import java.io.IOException;
import java.util.*;

public class WorldVisualisator implements IMapUpdateObserver {

    @Getter
    private final SimulationMap simulationMap;
    @Getter
    private final SimulationEngine simulationEngine;
    @Getter
    private final VBox container;
    private final GridPane mapGui;
    private boolean isRunning = false;
    @Getter
    private boolean showDominantAnimals = false;
    private Thread currentThread = null;

    private XYChart.Series animalsSeries, grassSeries, averageLifespanSeries, averageEnergySeries, averageChildrenSeries;

    private Text dayText, animalsText, grassText, avgLifespanText, avgEnergyText, avgChildrenText, dominantGenotype, magicHistory;
    private final CheckBox magicMapCheckbox;
    private VBox animalDetails;
    private final int delayInMillis;
    private final LinkedList<int[]> historicalData = new LinkedList<>();
    private final Button addAnimalButton, exportToCsvButton, dominantAnimalsButton;

    public WorldVisualisator(SimulationMap simulationMap, SimulationEngine simulationEngine, int delayInMillis) {
        this.simulationMap = simulationMap;
        this.simulationEngine = simulationEngine;
        this.delayInMillis = delayInMillis;
        this.simulationEngine.registerObserver(this);

        container = new VBox();

        VBox leftPanel = new VBox();
        leftPanel.getChildren().add(new Text(this.simulationMap.isSolidWalls() ? "Map with solid walls" : "Not solid walls"));

        magicMapCheckbox = new CheckBox("Is magic map?");

        addAnimalButton = createAddAnimalButton();
        exportToCsvButton = createExportToCsvButton();
        dominantAnimalsButton = createShowDominantButton();

        HBox buttons = new HBox();
        buttons.getChildren().addAll(getRunPauseButton(), addAnimalButton, exportToCsvButton, dominantAnimalsButton);
        leftPanel.getChildren().addAll(magicMapCheckbox, buttons);

        dominantGenotype = new Text();
        magicHistory = new Text();
        magicHistory.setVisible(false);

        leftPanel.getChildren().addAll(getStatsGridPane(), dominantGenotype, magicHistory, prepareChart());
        updateAnimalDetailedBox(null);
        leftPanel.getChildren().add(this.animalDetails);


        mapGui = generateMapGui();
        refreshMapGui();

        container.getChildren().addAll(leftPanel, mapGui);

    }

    private Button getRunPauseButton() {
        Button runPauseButton = new Button("Run simulation");
        runPauseButton.setOnAction((v) -> {
            isRunning = !isRunning;
            runPauseButton.setText(isRunning ? "Stop" : "Start");
            magicMapCheckbox.setDisable(true);
            simulationEngine.setMagic(this.magicMapCheckbox.isSelected());
            magicHistory.setVisible(simulationEngine.isMagic());
            exportToCsvButton.setDisable(isRunning);
            if(isRunning) {
                currentThread = new Thread(this.simulationEngine);
                currentThread.start();
            }else{
                this.simulationEngine.setRunning(false);
                currentThread.interrupt();
            }
        });
        return runPauseButton;
    }

    private GridPane generateMapGui() {
        GridPane gui = new GridPane();
        gui.setVgap(1);
        gui.setHgap(1);
        gui.setPrefSize(15, 15);
        ColumnConstraints columnConstraints = new ColumnConstraints(15);
        for(int i = 0; i < simulationMap.getWidth(); i++) {
            gui.getColumnConstraints().add(columnConstraints);
        }
        RowConstraints rowConstraints = new RowConstraints(15);
        for(int i = 0; i < simulationMap.getHeight(); i++) {
            gui.getRowConstraints().add(rowConstraints);
        }
        return gui;
    }

    @Synchronized
    private void refreshMapGui() {
        mapGui.getChildren().clear();
        for(int x = 0; x < simulationMap.getWidth(); x++) {
            for (int y = 0; y < simulationMap.getHeight(); y++) {
                Vector2D pos = new Vector2D(x, y);
                if(simulationMap.getMapElementsAt(pos).size() > 0) {
                    Optional<IMapElement> mapElement = simulationMap.getMapElementsAt(pos).stream().max(Comparator.comparing(IMapElement::getViewPriority));
                    if(mapElement.isPresent()) {
                        VBox vb = new GuiElementBox(this, mapElement.get()).getvBox();
                        mapGui.add(vb, x, y);
                    }
                }else {
                    if (simulationMap.isGrassOnPosition(pos)) {
                        VBox vb = new GuiElementBox(this, new Grass(pos)).getvBox();
                        mapGui.add(vb, x, y);
                    }else {
                        mapGui.add(getEmptyPositionImageView(pos), x, y);
                    }
                }
            }
        }
    }

    private ScatterChart prepareChart() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setAutoRanging(true);
        xAxis.setForceZeroInRange(false);
        xAxis.setLabel("Day");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Value");

        ScatterChart scatterChart = new ScatterChart(xAxis, yAxis);

        scatterChart.setPrefSize(640, 360);

        animalsSeries = getNewSeries("Animals");
        grassSeries = getNewSeries("Grass");
        averageLifespanSeries = getNewSeries("Avg lifespan");
        averageEnergySeries = getNewSeries("Avg energy");
        averageChildrenSeries = getNewSeries("Avg children");

        scatterChart.setLegendVisible(true);
        scatterChart.setAnimated(false);

        scatterChart.getData().addAll(animalsSeries, grassSeries, averageLifespanSeries, averageEnergySeries, averageChildrenSeries);

        return scatterChart;
    }

    @Override
    public void mapUpdated() {
        Platform.runLater(() -> {
            refreshMapGui();
            int day = this.simulationMap.getDay();
            int animals = this.simulationMap.getAnimals().size();
            int grass = this.simulationMap.getGrassAmount();
            int averageLifespan = simulationMap.getAverageLifespan();
            int averageEnergy = simulationMap.getAverageEnergy();
            int averageChildren = simulationMap.getAverageChildrenAmount();

            Map.Entry<List<AnimalRotation>, Integer> dominantGenotype = simulationMap.calculateDominantGenotype();
            animalsSeries.getData().add(new XYChart.Data(day, animals));
            grassSeries.getData().add(new XYChart.Data(day, grass));
            averageLifespanSeries.getData().add(new XYChart.Data(day, averageLifespan));
            averageEnergySeries.getData().add(new XYChart.Data(day, averageEnergy));
            averageChildrenSeries.getData().add(new XYChart.Data(day, averageChildren));
            if(animalsSeries.getData().size() > Config.getChartShownDays()) {
                clearOldestRecords();
            }
            int[] data = new int[] {day, animals, grass, averageEnergy, averageLifespan, averageChildren};
            historicalData.add(data);
            this.dayText.setText("Day: " + day);
            this.animalsText.setText("Animals: " + animals);
            this.grassText.setText("Grass: " + grass);

            this.avgChildrenText.setText("Avg children: " + averageChildren);
            this.avgEnergyText.setText("Avg energy: " + averageEnergy);
            this.avgLifespanText.setText("Avg lifespam: " + averageLifespan);

            if (dominantGenotype != null) {
                this.dominantGenotype.setText("Dominant genotype ["+dominantGenotype.getValue()+"]: " + dominantGenotype.getKey());
            }
            magicHistory.setText("Magic rescue days ("+this.simulationEngine.getMagicTries()+"/3): "
                    + Arrays.toString(this.simulationEngine.getMagicTriesHistory()));
            this.updateAnimalDetailedBox(this.simulationEngine.getObservedAnimal());
        });
        try {
            Thread.sleep(delayInMillis);
        } catch (InterruptedException e) {
            System.out.println("Simulation has been interrupted!");
        }
    }

    public void updateAnimalDetailedBox(Animal animal) {
        if (animalDetails == null) {
            this.animalDetails = new VBox();
        }
        if (animal == null) return;
        this.animalDetails.getChildren().clear();

        Label animalLabel = new Label("Observing #" + animal.getId() + " ("+ animal.getPosition()+")");
        Label energyLabel = new Label("Energy " + animal.getEnergy());
        Label bornDateLabel = new Label("Born date " + animal.getBornDate() + " death: " + animal.getDeathDate());
        Label childrenLabel = new Label("Total children: " + animal.getChildrenAmount() +
                ", descendants (from now): " + animal.getMap().getObservedAnimalDescendantAmount());

        Label genotype = new Label("Genotype: " + animal.getGenes());
        this.animalDetails.getChildren().addAll(animalLabel, energyLabel, bornDateLabel, childrenLabel, genotype);

    }

    private void saveHistoricalData() {
        if(this.historicalData.size() == 0) return;
        try {
            System.out.println("Writing simulation data to CSV file...");
            String fileName = simulationEngine.isMagic() ? "magic" : "default";
            fileName += simulationMap.isSolidWalls() ? "_solid" : "";
            WriterCSV writerCSV = new WriterCSV( fileName+".csv",
                    "day,animals,grass,average_energy,average_lifespan,average_children".split(","));

            int[] totalValue = new int[historicalData.get(0).length];
            for(int[] row : historicalData) {
                writerCSV.writeData(row);
                for(int i = 1; i < row.length; i++) {
                    totalValue[i] += row[i];
                }
            }

            for(int i = 0; i < totalValue.length; i++) {
                totalValue[i] /= historicalData.size();
            }

            writerCSV.writeData(totalValue);

            writerCSV.close();
            System.out.println("The data ("+historicalData.size()+" records) has been successfully saved to file!");
        } catch (IOException e) {
            System.out.println("Error occured during saving to CSV file: " + e.getMessage());
        }
    }

    private XYChart.Series getNewSeries(String name) {
        XYChart.Series result = new XYChart.Series();
        result.setName(name);
        result.getData().add(new XYChart.Data(0, 0));
        return result;
    }

    private void clearOldestRecords(){
        animalsSeries.getData().remove(0);
        grassSeries.getData().remove(0);
        averageLifespanSeries.getData().remove(0);
        averageEnergySeries.getData().remove(0);
        averageChildrenSeries.getData().remove(0);
    }

    public void stopSimulation() {
        if(isRunning) {
            this.currentThread.interrupt();
            this.isRunning = false;
        }
    }

    private ImageView getEmptyPositionImageView(Vector2D pos) {
        ImageView imageView = new ImageView(simulationMap.isJungle(pos) ? GuiUtils.getNotGrassJungle() : GuiUtils.getNotGrass());
        imageView.setFitHeight(20);
        imageView.setFitWidth(20);
        imageView.setOpacity(simulationMap.isJungle(pos) ? 0.5f : 0.2f);
        return imageView;
    }

    private GridPane getStatsGridPane(){
        GridPane stats = new GridPane();
        stats.setHgap(20);
        dayText = new Text();
        animalsText = new Text();
        grassText = new Text();
        stats.add(dayText, 1, 1);
        stats.add(animalsText, 2, 1);
        stats.add(grassText, 3, 1);

        avgLifespanText = new Text();
        avgEnergyText = new Text();
        avgChildrenText = new Text();

        stats.add(avgLifespanText, 1, 2);
        stats.add(avgEnergyText, 2, 2);
        stats.add(avgChildrenText, 3, 2);
        return stats;
    }

    private Button createAddAnimalButton() {
        Button addAnimalButton = new Button("Add animal to map");
        addAnimalButton.setOnAction((v) -> {
            this.simulationMap.spawnAnimalAtRandomPosition(Config.getStartEnergy());
            mapUpdated();
        });
        return addAnimalButton;
    }

    private Button createExportToCsvButton() {
        Button button = new Button("Export to CSV");
        button.setOnAction((v) -> saveHistoricalData());
        return button;
    }

    private Button createShowDominantButton() {
        Button button = new Button("Show dominant animals");
        button.setOnAction((e) -> {
            this.showDominantAnimals = !this.showDominantAnimals;
            button.setText((this.showDominantAnimals ? "Hide" : "Show") + " dominant animals");
            refreshMapGui();
        });
        return button;
    }

}
