package agh.ics.oop.simulation;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static final String MAIN_PATH = "src/main/resources/";

    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static boolean getRandomBoolean(float probabilityOfTrue) {
        return Math.random() < probabilityOfTrue;
    }

    public static String getCurrentTimeDateString() {
        return new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss").format(new Date());
    }

}
