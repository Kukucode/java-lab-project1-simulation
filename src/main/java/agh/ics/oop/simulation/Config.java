package agh.ics.oop.simulation;

import lombok.Getter;
import lombok.Setter;

public class Config {

    @Getter @Setter
    public static int startEnergy, moveEnergy, plantEnergy, chartShownDays, startAmountOfFood;

}
