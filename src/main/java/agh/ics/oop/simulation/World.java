package agh.ics.oop.simulation;

import agh.ics.oop.simulation.gui.App;
import javafx.application.Application;

public class World {

    public static void main(String[] args) {
        System.out.println("Starting simulator...");
        Application.launch(App.class);
    }

}
