package agh.ics.oop.simulation;

import agh.ics.oop.simulation.map.IMapUpdateObserver;
import agh.ics.oop.simulation.map.SimulationMap;
import agh.ics.oop.simulation.map.Animal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Synchronized;

import java.util.ArrayList;

@RequiredArgsConstructor
public class SimulationEngine implements Runnable {

    private final SimulationMap map;

    @Getter
    @Setter
    private boolean isMagic;

    @Getter
    private int magicTries = 0;
    @Getter
    private final int[] magicTriesHistory = new int[3];

    private final ArrayList<IMapUpdateObserver> observers = new ArrayList<>();

    @Getter
    @Setter
    private boolean isRunning = false;
    private Animal observedAnimal;

    @Override
    @Synchronized
    public void run() {
        isRunning = true;
        while(isRunning) {
            removeAnimalsFood();
            if(map.getAnimals().size() <= 5 && getMagicTries() < 3) {
                respawnMagicAnimals();
            }

            this.map.getAnimals().forEach((a) -> a.eat());
            this.map.getAnimals().forEach((a) -> a.breed());
            this.map.getAnimals().forEach((a) -> a.setHasBreededThisDay(false));
            this.map.generateAnimalsInQueue(false);

            this.map.spawnFood(2);
            this.map.setNextDay();
            this.notifyObservers();
        }
    }

    public void registerObserver(IMapUpdateObserver observer) {
        this.observers.add(observer);
    }

    public void notifyObservers() {
        for(IMapUpdateObserver observer : observers) {
            observer.mapUpdated();
        }
    }

    public Animal getObservedAnimal() {
        return observedAnimal;
    }

    public void setObservedAnimal(Animal observedAnimal) {
        this.observedAnimal = observedAnimal;
    }

    private void removeAnimalsFood() {
        ArrayList<Animal> animalsToRemove = new ArrayList<>();
        for(Animal a : this.map.getAnimals()) {
            a.setEnergy(a.getEnergy()-Config.getMoveEnergy());
            if(a.getEnergy() <= 0) {
                animalsToRemove.add(a);
            }else {
                a.move();
            }
        }
        for(Animal a : animalsToRemove) {
            a.setDeathDate(map.getDay());
            this.map.addToTotalLifespan(a.getLifespan());
            map.removeAnimal(a);
        }
        this.map.addToTotalDeaths(animalsToRemove.size());
    }

    private void respawnMagicAnimals() {
        this.magicTriesHistory[this.magicTries] = this.map.getDay();
        this.magicTries++;
        for(Animal animal : this.map.getAnimals()) {
            Animal toSpawn = new Animal(animal);
            toSpawn.setPosition(this.map.getRandomFreePosition());
            map.addAnimalToGenerateQueue(toSpawn);
        }
        map.generateAnimalsInQueue(true);

    }

}
