package agh.ics.oop.simulation;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class WriterCSV {

    private final CSVWriter writer;

    public WriterCSV(String flatFilename, String[] header) throws IOException {
        String directoryPath = "results/";
        File directory = new File(directoryPath);
        if (! directory.exists()) {
            directory.mkdir();
        }
        writer = new CSVWriter(new FileWriter(directoryPath + Utils.getCurrentTimeDateString()+"_" +flatFilename));
        writer.writeNext(header);
    }

    public void writeData(int[] values) {
        String[] toWrite = Arrays.stream(values).mapToObj(String::valueOf).toArray(String[]::new);
        writer.writeNext(toWrite);
    }

    public void close() throws IOException {
        writer.close();
    }

}
