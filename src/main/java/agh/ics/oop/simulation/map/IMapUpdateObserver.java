package agh.ics.oop.simulation.map;

public interface IMapUpdateObserver {

    void mapUpdated();

}
