package agh.ics.oop.simulation.map;

import javafx.scene.image.Image;

public interface IMapElement {

    Vector2D getPosition();
    boolean isAt(Vector2D position);
    String getImagePath();
    Image getImage();
    int getViewPriority();

}
