package agh.ics.oop.simulation.map;

import agh.ics.oop.simulation.Config;
import agh.ics.oop.simulation.Utils;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.Setter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class Animal implements IMapElement, Comparable {

    private final SimulationMap map;
    @Getter @Setter
    private Vector2D position;
    @Getter @Setter
    private AnimalRotation rotation;
    @Getter
    private final ArrayList<AnimalRotation> genes;
    private final ArrayList<IAnimalPositionChangeObserver> observers = new ArrayList<>();
    @Getter @Setter
    private boolean hasBreededThisDay = false, isObserved = false;
    private Image image;

    @Getter
    private final int id, bornDate;
    @Getter @Setter
    private int energy, deathDate;
    @Getter
    private int childrenAmount = 0;

    public Animal(SimulationMap map, Vector2D position, int energy, AnimalRotation rotation) {
        this.map = map;
        this.position = position;
        this.energy = energy;
        this.rotation = rotation;
        genes = new ArrayList<>();
        for(int i = 0; i < 32; i++) {
            genes.add(AnimalRotation.getRandom());
        }
        this.id = map.getNextAnimalID();
        this.bornDate = map.getDay();
        Collections.sort(this.genes);
    }

    public Animal(Animal parent1, Animal parent2) {
        this.map = parent1.getMap();
        this.position = parent1.position;
        this.rotation = AnimalRotation.getRandom();
        int e1 = parent1.getEnergy();
        int e2 = parent2.getEnergy();

        this.energy = (e1 + e2)/4;
        parent1.setEnergy((int) ((float) e1*(0.75f)));
        parent2.setEnergy((int) ((float) e2*(0.75f)));

        parent1.increaseChildrenAmount();
        parent2.increaseChildrenAmount();

        this.setObserved(parent1.isObserved() || parent2.isObserved());
        if(this.isObserved()) {
            map.increaseObservedAnimalDescendantAmount(1);
        }

        this.id = map.getNextAnimalID();
        this.bornDate = map.getDay();

        boolean p1left = Utils.getRandomBoolean(0.5f);

        int cut = (e1) * 32 / (e1+e2);

        if (p1left) cut = 32 - cut;

        genes = new ArrayList<>();
        for(int i = 0; i < 32; i++) {
            if (p1left ^ (i < cut)) {
                this.genes.add(parent1.getGenes().get(i));
            }else {
                this.genes.add(parent2.getGenes().get(i));
            }
        }

        hasBreededThisDay = true;
        parent1.hasBreededThisDay = true;
        parent2.hasBreededThisDay = true;

        Collections.sort(this.genes);
    }

    public Animal(Animal copiedFrom) {
        this.map = copiedFrom.getMap();
        this.position = copiedFrom.getPosition();
        this.energy = Config.getStartEnergy();
        this.rotation = AnimalRotation.getRandom();
        this.genes = copiedFrom.getGenes();
        this.bornDate = copiedFrom.getBornDate();
        this.id = map.getNextAnimalID();
    }

    public boolean isAt(Vector2D position) {
        return position.equals(this.position);
    }

    public String getImagePath() {
        return Utils.MAIN_PATH + "grass.png";
    }

    @Override
    public Image getImage() {
        if (this.image == null) {
            try {
                image = new Image(new FileInputStream(this.getImagePath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return this.image;
    }

    @Override
    public int getViewPriority() {
        return this.getEnergy();
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Animal animal){
            if(animal.getEnergy() == this.getEnergy()) {
                return animal.getId() - this.getId();
            }
            return this.energy - animal.energy;
        }
        return 1;
    }

    public void move() {
        int moveNumber = Utils.getRandomNumber(0, 32);
        AnimalRotation nextMove = genes.get(moveNumber);
        Vector2D newPosition = null;
        if(!nextMove.isMovingRotation()) {
            this.rotation = this.rotation.rotate(nextMove);
            return;
        }

        if (nextMove == AnimalRotation.FRONT) {
            newPosition = this.position.add(rotation.getMovement());
        }else if (nextMove == AnimalRotation.BACK) {
            newPosition = this.position.add(rotation.getMovement().opposite());
        }

        Vector2D oldPosition = this.position;
        if(map.isSolidWalls() && !map.canMoveTo(newPosition)) {
            return;
        }

        newPosition = new Vector2D(Math.floorMod(newPosition.x, map.getWidth()),
                Math.floorMod(newPosition.y, map.getHeight()));

        this.position = newPosition;
        notifyObservers(oldPosition);
    }

    public void eat() {
        if(!map.isGrassOnPosition(this.position)) return;
        HashSet<Animal> elementsAtPos = this.map.getAnimalsAt(this.position);
        Optional<Animal> mostEnergyAnimal = elementsAtPos
                .stream().max(Comparator.comparing(Animal::getEnergy));

        List<Animal> highestEnergyAnimals = elementsAtPos
                .stream().filter((superAnimal -> superAnimal.getEnergy() == mostEnergyAnimal.get().getEnergy())).toList();

        int foodForEachAnimal = Config.getPlantEnergy() / highestEnergyAnimals.size();

        highestEnergyAnimals.forEach((superAnimal -> superAnimal.addEnergy(foodForEachAnimal)));
        map.setGrassOnPosition(this.position, false);
    }

    public void breed() {
        HashSet<Animal> elementsAtPos = this.map.getAnimalsAt(this.position);
        if(elementsAtPos.size() < 2) return;
        List<Animal> sa = new ArrayList<>(elementsAtPos);
        Collections.sort(sa);
        Animal parent1 = sa.get(elementsAtPos.size()-1);
        Animal parent2 = sa.get(elementsAtPos.size()-2);

        if(parent1.hasBreededThisDay || parent2.hasBreededThisDay) return;

        int minimalEnergy = Config.getStartEnergy() / 2;
        if(parent1.getEnergy() < minimalEnergy || parent2.getEnergy() < minimalEnergy) return;

        Animal child = new Animal(parent1, parent2);
        this.map.addAnimalToGenerateQueue(child);

   }

    @Override
    public String toString() {
        return ""+this.energy;
    }

    public void registerObserver(IAnimalPositionChangeObserver observer) {
        this.observers.add(observer);
    }

    public void notifyObservers(Vector2D oldPosition) {
        for(IAnimalPositionChangeObserver observer : this.observers) {
            observer.positionChanged(this, oldPosition);
        }
    }

    public SimulationMap getMap() {
        return map;
    }

    public void addEnergy(int amount) {
        this.setEnergy(this.getEnergy()+amount);
    }

    public int getLifespan() {
        return Math.max(deathDate, map.getDay()) - bornDate;
    }

    public void increaseChildrenAmount() {
        this.childrenAmount++;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal that = (Animal) o;
        return energy == that.energy && id == that.id;
    }



}
