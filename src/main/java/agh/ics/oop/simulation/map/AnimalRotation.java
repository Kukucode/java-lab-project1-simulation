package agh.ics.oop.simulation.map;

import agh.ics.oop.simulation.Utils;

public enum AnimalRotation {

    FRONT(new Vector2D(0, 1)),
    RIGHT_45(new Vector2D(1, 1)),
    RIGHT_90(new Vector2D(1, 0)),
    RIGHT_135(new Vector2D(1, -1)),
    BACK(new Vector2D(0, -1)),
    LEFT_135(new Vector2D(-1, -1)),
    LEFT_90(new Vector2D(-1, 0)),
    LEFT_45(new Vector2D(-1, 1));

    private final Vector2D movement;

    AnimalRotation(Vector2D movement) {
        this.movement = movement;
    }

    private static final AnimalRotation[] values = values();

    public AnimalRotation rotate(AnimalRotation animalRotation) {
        return values[(this.ordinal()+ animalRotation.ordinal())%values.length];
    }

    public static AnimalRotation getRandom() {
        return values[Utils.getRandomNumber(0, values.length)];
    }

    public Vector2D getMovement() {
        return movement;
    }

    public boolean isMovingRotation(){
        return this == AnimalRotation.FRONT || this == AnimalRotation.BACK;
    }

    @Override
    public String toString() {
        return String.valueOf(this.ordinal());
    }
}
