package agh.ics.oop.simulation.map;

public interface IAnimalPositionChangeObserver {

    void positionChanged(IMapElement mapElement, Vector2D oldPosition);

}
