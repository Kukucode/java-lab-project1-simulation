package agh.ics.oop.simulation.map;

import agh.ics.oop.simulation.Utils;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.NonNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Grass implements IMapElement {

    @Getter
    private final Vector2D position;
    private static Image image;

    public Grass(@NonNull Vector2D position) {
        this.position = position;
    }

    @Override
    public boolean isAt(Vector2D position) {
        return position.equals(this.position);
    }

    @Override
    public String toString() {
        return "*";
    }

    @Override
    public String getImagePath() {
        return Utils.MAIN_PATH + "grass2.png";
    }

    @Override
    public Image getImage() {
        if (image == null) {
            try {
                image = new Image(new FileInputStream(this.getImagePath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return image;
    }

    @Override
    public int getViewPriority() {
        return -1;
    }

}
