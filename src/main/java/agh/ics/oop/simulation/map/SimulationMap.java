package agh.ics.oop.simulation.map;

import agh.ics.oop.simulation.Utils;
import lombok.Getter;

import java.util.*;

public class SimulationMap implements IAnimalPositionChangeObserver {

    @Getter
    private final int width, height;
    private final float jungleRatio;


    private final Vector2D jungleBottomLeft, jungleTopRight;

    private final HashMap<Vector2D, HashSet<IMapElement>> elementsMap = new HashMap<>();
    private final boolean[][] grassMap;
    private final ArrayList<Animal> animals = new ArrayList<>();
    private final ArrayList<Animal> animalsToGenerateQueue = new ArrayList<>();
    @Getter
    private final boolean solidWalls;

    private int nextAnimalID = 0;
    @Getter
    private int day = 0;

    private int totalDeaths = 0, totalLifespans = 0;

    @Getter
    private int observedAnimalDescendantAmount = 0;

    @Getter
    private List<AnimalRotation> dominantGenotype;

    public SimulationMap(int width, int height, float jungleRatio, boolean solidWalls) {
        this.width = width;
        this.height = height;
        this.jungleRatio = jungleRatio;
        this.solidWalls = solidWalls;
        this.grassMap = new boolean[width][height];
        this.jungleBottomLeft = new Vector2D((int) ((1-jungleRatio)*width/2), (int) ((1-jungleRatio)*height/2));
        this.jungleTopRight = new Vector2D((int) ((1+jungleRatio)*width/2), (int) ((1+jungleRatio)*height/2));
    }

    public SimulationMap(int width, int height, float jungleRatio) {
        this(width, height, jungleRatio, false);
    }

    public void spawnManyAnimals(int amount, int energy) {
        for(int i = 0; i < amount; i++) {
            spawnAnimalAtRandomPosition(energy);
        }
    }

    public void spawnAnimalAtRandomPosition(Animal animal) {
        Vector2D pos = getRandomFreePosition();
        animal.setPosition(pos);
        animal.setRotation(AnimalRotation.getRandom());
        spawnAnimal(animal);
    }

    public void spawnAnimalAtRandomPosition(int energy){
        Animal sa = new Animal(this, null, energy, null);
        spawnAnimalAtRandomPosition(sa);
    }

    public void spawnAnimal(Animal animal){
        place(animal);
        animals.add(animal);
        animal.registerObserver(this);
    }

    public void spawnFood(int amount) {
        for (int i = 0; i < amount/2; i++) {
            spawnJungleFood();
            spawnNonJungleFood();
        }
    }

    public void spawnJungleFood() {
        Vector2D pos = null;


        int maxTries = 100;
        while(pos == null || getMapElementsAt(pos).size() > 0 || isGrassOnPosition(pos)) {
            int x = Utils.getRandomNumber(jungleBottomLeft.x, jungleTopRight.x+1);
            int y = Utils.getRandomNumber(jungleBottomLeft.y, jungleTopRight.y+1);

            pos = new Vector2D(x, y);
            maxTries--;
            if(maxTries<0) return;
        }

        setGrassOnPosition(pos, true);
    }

    public void spawnNonJungleFood() {
        Vector2D pos = null;
        int maxTries = 100;
        while(pos == null || getMapElementsAt(pos).size() > 0 || isGrassOnPosition(pos) || isJungle(pos)) {
            int x = Utils.getRandomNumber(0, width);
            int y = Utils.getRandomNumber(0, height);

            pos = new Vector2D(x, y);
            maxTries--;

            if(maxTries < 0) return;
        }

        setGrassOnPosition(pos, true);
    }

    public boolean canMoveTo(Vector2D position) {
        return position.x < width && position.x >= 0 && position.y >= 0 && position.y < height;
    }

    public void place(IMapElement mapElement) {
        if(mapElement instanceof Grass) {
            setGrassOnPosition(mapElement.getPosition(), true);
            return;
        }
        if (!elementsMap.containsKey(mapElement.getPosition())) {
            elementsMap.put(mapElement.getPosition(), new HashSet<>());
        }
        elementsMap.get(mapElement.getPosition()).add(mapElement);
    }

    public boolean isJungle(Vector2D position) {
        return position.follows(jungleBottomLeft) && position.precedes(jungleTopRight);
    }

    public HashSet<IMapElement> getMapElementsAt(Vector2D pos) {
        if (this.elementsMap.containsKey(pos)) {
            return this.elementsMap.get(pos);
        }
        return new HashSet<>();
    }

    public HashSet<Animal> getAnimalsAt(Vector2D pos) {
        HashSet<Animal> sa = new HashSet<>();
        for(IMapElement mapElement : getMapElementsAt(pos)) {
            if(mapElement instanceof Animal){
                sa.add((Animal) mapElement);
            }
        }
        return sa;
    }

    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    @Override
    public void positionChanged(IMapElement mapElement, Vector2D oldPosition) {
        this.getMapElementsAt(oldPosition).remove(mapElement);
        place(mapElement);
    }

    public void removeAnimal(Animal animal) {
        this.animals.remove(animal);
        this.getMapElementsAt(animal.getPosition()).remove(animal);
    }

    public boolean isGrassOnPosition(Vector2D pos) {
        return grassMap[pos.x][pos.y];
    }

    public void setGrassOnPosition(Vector2D pos, boolean value){
        grassMap[pos.x][pos.y] = value;
    }

    public int getGrassAmount() {
        int counter = 0;
        for(int i = 0; i < width; i++){
            for (int j = 0; j < height; j++) {
                if(grassMap[i][j]) counter++;
            }
        }
        return counter;
    }

    /**
     * Dodawanie do kolejki zwierzat nowo urodzonych. Po zakonczeniu dnia sa generowane a kolejka czyszczona
     * @param animal zwierze do dodania
     */
    public void addAnimalToGenerateQueue(Animal animal) {
        this.animalsToGenerateQueue.add(animal);
    }

    public void generateAnimalsInQueue(boolean randomPosition) {
        for (Animal animal : animalsToGenerateQueue) {
            if(randomPosition) {
                this.spawnAnimalAtRandomPosition(animal);
            }else {
                this.spawnAnimal(animal);
            }
        }
        this.animalsToGenerateQueue.clear();
    }

    public int getNextAnimalID() {
        this.nextAnimalID += 1;
        return this.nextAnimalID;
    }

    public void setNextDay() {
        this.day++;
    }

    public int getAverageLifespan() {
        return totalLifespans / (totalDeaths > 0 ? totalDeaths : 1);
    }

    public int getAverageEnergy() {
        if (animals.size() == 0){
            return 0;
        }
        return animals.stream().mapToInt(Animal::getEnergy).sum()/animals.size();
    }

    public int getAverageChildrenAmount() {
        if (animals.size() == 0){
            return 0;
        }
        return animals.stream().mapToInt(Animal::getChildrenAmount).sum()/animals.size();
    }

    public void addToTotalLifespan(int amount) {
        this.totalLifespans += amount;
    }

    public void addToTotalDeaths(int amount){
        this.totalDeaths += amount;
    }

    public void increaseObservedAnimalDescendantAmount(int amount) {
        this.observedAnimalDescendantAmount += amount;
    }

    public void setObservedAnimalDescendantAmount(int observedAnimalDescendantAmount) {
        this.observedAnimalDescendantAmount = observedAnimalDescendantAmount;
    }

    public Map.Entry<List<AnimalRotation>, Integer> calculateDominantGenotype() {
        HashMap<List<AnimalRotation>, Integer> genotypes = new HashMap<>();
        for(Animal sa : this.getAnimals()) {
            List<AnimalRotation> list = sa.getGenes();
            if(genotypes.containsKey(list)){
                genotypes.put(list, genotypes.get(list) + 1);
            }else {
                genotypes.put(list, 1);
            }
        }
        Map.Entry<List<AnimalRotation>, Integer> highestValue = null;
        for(Map.Entry<List<AnimalRotation>, Integer> mapEntry : genotypes.entrySet()) {
            if(highestValue == null || mapEntry.getValue() > highestValue.getValue()) {
                highestValue = mapEntry;
            }
        }
        if(highestValue != null) {
            dominantGenotype = highestValue.getKey();
        }
        return highestValue;
    }

    public Vector2D getRandomFreePosition() {
        Vector2D pos = null;
        while(pos == null || this.getAnimalsAt(pos).size() > 0) {
            pos = new Vector2D(Utils.getRandomNumber(0, width), Utils.getRandomNumber(0, height));
        }
        return pos;
    }
}
