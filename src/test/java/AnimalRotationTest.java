import agh.ics.oop.simulation.map.AnimalRotation;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnimalRotationTest {

    @Test
    public void testRotationRight45() {
        assertEquals(AnimalRotation.RIGHT_45, AnimalRotation.FRONT.rotate(AnimalRotation.RIGHT_45));
    }

    @Test
    public void testRotationLeft45() {
        assertEquals(AnimalRotation.LEFT_45, AnimalRotation.FRONT.rotate(AnimalRotation.LEFT_45));
    }

    @Test
    public void testRotation90() {
        assertEquals(AnimalRotation.BACK, AnimalRotation.RIGHT_90.rotate(AnimalRotation.RIGHT_90));
        assertEquals(AnimalRotation.RIGHT_45, AnimalRotation.LEFT_45.rotate(AnimalRotation.RIGHT_90));
        assertEquals(AnimalRotation.LEFT_45, AnimalRotation.RIGHT_45.rotate(AnimalRotation.LEFT_90));
    }

    @Test
    public void testRotation135() {
        assertEquals(AnimalRotation.RIGHT_45, AnimalRotation.LEFT_90.rotate(AnimalRotation.RIGHT_135));
        assertEquals(AnimalRotation.FRONT, AnimalRotation.LEFT_135.rotate(AnimalRotation.RIGHT_135));
    }

}
