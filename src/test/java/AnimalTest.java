
import agh.ics.oop.simulation.Config;
import agh.ics.oop.simulation.map.SimulationMap;
import agh.ics.oop.simulation.map.Animal;
import agh.ics.oop.simulation.map.AnimalRotation;
import agh.ics.oop.simulation.map.Vector2D;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnimalTest {

    @Test
    public void testNewChildBorn() {
        SimulationMap map = new SimulationMap(10, 10, 0.1f);
        Config.setStartEnergy(20);
        Animal parent1 = new Animal(map, new Vector2D(5, 5), Config.getStartEnergy(), AnimalRotation.getRandom());
        Animal parent2 = new Animal(map, new Vector2D(5, 5), Config.getStartEnergy(), AnimalRotation.getRandom());

        Animal child = new Animal(parent1, parent2);
        assertEquals(15, parent1.getEnergy());
        assertEquals(15, parent2.getEnergy());
        assertEquals(10, child.getEnergy());

        assertEquals(parent1.getPosition(), child.getPosition());
        assertEquals(parent2.getPosition(), child.getPosition());
    }

    @Test
    public void testEatFood() {
        SimulationMap map = new SimulationMap(10, 10, 0.1f);
        Vector2D pos = new Vector2D(5, 5);
        Config.setStartEnergy(20);
        Config.setPlantEnergy(20);
        Animal animal1 = new Animal(map, pos, 20, AnimalRotation.getRandom());
        Animal animal2 = new Animal(map, pos, 20, AnimalRotation.getRandom());
        Animal animal3 = new Animal(map, pos, 10, AnimalRotation.getRandom());

        map.addAnimalToGenerateQueue(animal1);
        map.addAnimalToGenerateQueue(animal2);
        map.addAnimalToGenerateQueue(animal3);
        map.generateAnimalsInQueue(false);

        animal1.eat();

//        Nie ma trawy
        assertEquals(20, animal1.getEnergy());
        assertEquals(20, animal2.getEnergy());
        assertEquals(10, animal3.getEnergy());

//        Dwa najmocniejsze zjadaja trawe po równo
        map.setGrassOnPosition(pos, true);
        animal1.eat();

        assertEquals(30, animal1.getEnergy());
        assertEquals(30, animal2.getEnergy());
        assertEquals(10, animal3.getEnergy());
    }

}
